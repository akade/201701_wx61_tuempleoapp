﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TuEmpleo.Models;
using TuEmpleo.Controllers;
using TuEmpleo.ViewModel;

namespace TuEmpleo.Test
{
    [TestFixture]
    public class MyTest
    {
        public TuEmpleoEntities context = new TuEmpleoEntities();

        [Test]
        public void ValidarPersona()
        {
            HomeController controller = new HomeController();            
            UsuarioViewModel objViewModel = new UsuarioViewModel();
            objViewModel.Correo = "CALVO";
            objViewModel.Contraseña = "CALVO";
            var bd = context.Persona.Where(x => x.Correo == objViewModel.Correo && x.Contraseña == objViewModel.Contraseña).FirstOrDefault();
            
            Assert.True(Convert.ToBoolean(bd.PersonaId));
        }
        [Test]
        public void RegistrarUsuario()
        {
            HomeController controller = new HomeController();
            UsuarioViewModel objViewModel = new UsuarioViewModel();
            objViewModel.Nombre = "Name";
            objViewModel.Apellido = "Name";
            objViewModel.Dni = "77333";
            objViewModel.FechaNacimiento = "2010-10-10";
            objViewModel.Correo = "PR@gmail.com";
            objViewModel.Contraseña = "Name";
            
            objViewModel.GuardarPersona();

            objViewModel.Usuario = "Ultimo";
            objViewModel.Titulo = "Ingeniero SW";
            objViewModel.Descripcion = "DDDDDDDDDDDDDDDDDDDDDDDDDDDESCP";
            objViewModel.CobroHora = "12";
            objViewModel.GuardarPerfil();
            var perfil = context.Perfil.OrderByDescending(x=>x.PerfilId).FirstOrDefault();
            Assert.That(objViewModel.PerfilId, Is.EqualTo(perfil.PerfilId));
            var person = context.Persona.Where(x => x.PersonaId == perfil.PersonaId).FirstOrDefault();
            context.Perfil.Remove(perfil);
            context.Persona.Remove(person);
        }
        
        [Test]
        public void AgregarProyecto()
        {
            UserController controller = new UserController();
            ProyectoUsuarioViewModel objViewModel = new ProyectoUsuarioViewModel();
            var persona = context.Persona.Where(x => x.PersonaId == 53).FirstOrDefault();
            objViewModel.persona = persona;
            objViewModel.PersonaID = persona.PersonaId;
            objViewModel.Titulo = "Newew2";
            objViewModel.Area = "Construccion";
            objViewModel.Descripcion = "Descripcion";
            objViewModel.RangoPago = "12";
            objViewModel.GuardarProyecto();
            var proyect = context.Proyecto.OrderByDescending(x => x.ProyectoId).FirstOrDefault();
            Assert.That(objViewModel.proyecto.ProyectoId, Is.EqualTo(proyect.ProyectoId));
        }
        
        [Test]
        public void EditarProyecto()
        {
            UserController controller = new UserController();
            ProyectoUsuarioViewModel objViewModel = new ProyectoUsuarioViewModel();
            var persona = context.Persona.Where(x => x.PersonaId == 53).FirstOrDefault();
            //var proyecto = context.Proyecto.Where(x => x.ProyectoId == 24).FirstOrDefault();
            objViewModel.persona = persona;
            objViewModel.ProyectoID = 24;
            objViewModel.PersonaID = 53;
            if (objViewModel.ValidarCreador())
            {
                objViewModel.Titulo = "EWWWWWW";
                objViewModel.Area = "NUEVONOMBRE";
                objViewModel.Descripcion = "NUEVONOMBRE";
                objViewModel.RangoPago = "100";
                //controller.GuardarProyecto(objViewModel);
                objViewModel.GuardarProyecto();
                var proyecto = context.Proyecto.Where(x => x.ProyectoId == 24).FirstOrDefault();
                Assert.That(proyecto.Titulo, Is.EqualTo(objViewModel.Titulo));
            }
        }
        
        [Test]
        public void AgregarPostulacion()
        {
            UserController controller = new UserController();
            ProyectoDetalleViewModel objViewModel = new ProyectoDetalleViewModel();
            var proyecto = context.Proyecto.Where(x => x.ProyectoId == 24).FirstOrDefault();
            objViewModel.UsuarioActual = context.Perfil.Where(x => x.PersonaId == 19).FirstOrDefault();

            objViewModel.MensajePostulacion = "SSSSS";
            objViewModel.Proyecto = proyecto;
            objViewModel.AgregarPostulacion();
            ProyectoPersona pp = context.ProyectoPersona.OrderByDescending(x => x.PersonaId == objViewModel.UsuarioActual.PersonaId &&
                                                                x.ProyectoId == proyecto.ProyectoId).FirstOrDefault();
            Assert.That(objViewModel.MensajePostulacion, Is.EqualTo(pp.Mensaje));
            context.ProyectoPersona.Remove(pp);
        }
    }
}