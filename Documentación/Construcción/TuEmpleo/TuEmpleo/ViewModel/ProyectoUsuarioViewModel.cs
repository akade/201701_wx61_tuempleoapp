﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using TuEmpleo.Models;

namespace TuEmpleo.ViewModel
{
    public class ProyectoUsuarioViewModel
    {
        public Persona persona { get; set; }
        public Perfil perfil { get; set; }
        public Proyecto proyecto { get; set; }
        public ProyectoPersona proyectoPersona { get; set; }

        public int PersonaID { get; set; }
        public int ProyectoID { get; set; }
        public String Titulo { get; set; }
        public String Area { get; set; }
        public String Descripcion { get; set; }
        public String RangoPago { get; set; }

        public ProyectoUsuarioViewModel()
        {
            persona = new Persona();
            proyecto = new Proyecto();
            proyectoPersona = new ProyectoPersona();

        }
        public bool ValidarCreador()
        {
            TuEmpleoEntities context = new TuEmpleoEntities();
            proyectoPersona = context.ProyectoPersona.Where(x => x.PersonaId == PersonaID && x.ProyectoId == ProyectoID && x.TipoPersonaId == 10).FirstOrDefault();
            if (proyectoPersona == null) return false;
            return true;
        }

        public void GuardarProyecto()
        {
            TuEmpleoEntities context = new TuEmpleoEntities();

            if (Titulo != null && ValidarTexto(Titulo) &&
                Area != null && ValidarTexto(Area) &&
                RangoPago != null && ValidarNumeros(RangoPago) &&
                Descripcion != null && ValidarTexto(Descripcion))
            {
                if (ProyectoID == 0)
                {
                    proyecto.Titulo = Titulo;
                    proyecto.Area = Area;
                    proyecto.Descripcion = Descripcion;
                    proyecto.RangoPago = RangoPago;
                    context.Proyecto.Add(proyecto);
                    context.SaveChanges();

                    proyectoPersona.ProyectoId = proyecto.ProyectoId;
                    proyectoPersona.PersonaId = PersonaID;
                    proyectoPersona.TipoPersonaId = 10;
                    context.ProyectoPersona.Add(proyectoPersona);
                    context.SaveChanges();
                    ProyectoID = proyectoPersona.ProyectoId;
                }
                else
                {
                    proyecto = context.Proyecto.Where(x => x.ProyectoId == ProyectoID).FirstOrDefault();
                    proyecto.Titulo = Titulo;
                    proyecto.Area = Area;
                    proyecto.Descripcion = Descripcion;
                    proyecto.RangoPago = RangoPago;
                    context.SaveChanges();

                }
            }
        }

        public void ObtenerProyecto(int id)
        {
            TuEmpleoEntities contexto = new TuEmpleoEntities();
            proyecto = contexto.Proyecto.Where(x => x.ProyectoId == id).FirstOrDefault();
            Titulo = proyecto.Titulo;
            Area = proyecto.Area;
            Descripcion = proyecto.Descripcion;
            RangoPago = proyecto.RangoPago;
            ProyectoID = proyecto.ProyectoId;
        }
        public bool ValidarTexto(String text)
        {
            if (Regex.IsMatch(text, @"^[a-zA-Z ]+$"))
            {
                return true;
            }
            return false;
        }

        public bool ValidarNumeros(String text)
        {
            if (Regex.IsMatch(text, @"^[0-9]+$"))
            {
                return true;
            }
            return false;
        }

    }
}