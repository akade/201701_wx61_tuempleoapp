﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TuEmpleo.Models;

namespace TuEmpleo.ViewModel
{
    public class ProyectoPuestoViewModel
    {
        public List<Proyecto> ListaProyectos { get; set; }
        public List<ProyectoPersona> ListaProyectoPersona { get; set; }
        public int Cantidad { get; set; }
        public Persona persona { get; set; }
        public ProyectoPuestoViewModel()
        {
            TuEmpleoEntities context = new TuEmpleoEntities();
            ListaProyectoPersona = new List<ProyectoPersona>();
            ListaProyectos = context.Proyecto.ToList();
            Cantidad = ListaProyectos.Count();
        }
        public void ListaPorUsuario()
        {
            TuEmpleoEntities context = new TuEmpleoEntities();
            ListaProyectos = new List<Proyecto>();
            ListaProyectoPersona = context.ProyectoPersona.Where(x => x.PersonaId == persona.PersonaId && x.TipoPersonaId == 11).ToList();
            foreach (var p in ListaProyectoPersona)
            {
                Proyecto add = context.Proyecto.Where(x => x.ProyectoId == p.ProyectoId).FirstOrDefault();
                ListaProyectos.Add(add);
            }
            Cantidad = ListaProyectos.Count();
        }
        public void ListaCreados()
        {
            TuEmpleoEntities context = new TuEmpleoEntities();
            ListaProyectos = new List<Proyecto>();
            ListaProyectoPersona = context.ProyectoPersona.Where(x => x.PersonaId == persona.PersonaId && x.TipoPersonaId == 10).ToList();
            foreach (var p in ListaProyectoPersona)
            {
                Proyecto add = context.Proyecto.Where(x => x.ProyectoId == p.ProyectoId).FirstOrDefault();
                ListaProyectos.Add(add);
            }
            Cantidad = ListaProyectos.Count();
        }

    }
}