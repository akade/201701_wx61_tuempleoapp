﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using TuEmpleo.Models;

namespace TuEmpleo.ViewModel
{
    public class UsuarioViewModel
    {
        public int PersonaId { get; set; }
        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public String Dni { get; set; }
        public String Correo { get; set; }
        public String Contraseña { get; set; }
        
        public String FechaNacimiento { get; set; }
        public int PerfilId { get; set; }
        public Perfil perfil { get; set; }
        public Persona persona { get; set; }
        public String Usuario { get; set; }
        public String Titulo { get; set; }
        public String Descripcion { get; set; }
        public String CobroHora { get; set; }


        public UsuarioViewModel()
        {

            perfil = new Perfil();
            persona = new Persona();
        }

        public void GuardarPersona()
        {
            
            if (ValidarTexto(Nombre) && Nombre != null
                && ValidarTexto(Apellido) && Apellido != null
                && Contraseña != null
                && ValidarCorreo(Correo) && Correo != null
                && ValidarNumeros(Dni) && Dni != null
                && ValidarFecha(FechaNacimiento) && FechaNacimiento != null)
            {
                TuEmpleoEntities context = new TuEmpleoEntities();
                persona.Nombre = Nombre;
                persona.Apellido = Apellido;
                persona.Contraseña = Contraseña;
                persona.Correo = Correo;
                persona.Dni = Dni;
                persona.FechaNacimiento = Convert.ToDateTime(FechaNacimiento);
                context.Persona.Add(persona);
                context.SaveChanges();
                PersonaId = persona.PersonaId;
            }
            else
            {
                return;
            }
        }


        public void GuardarPerfil()
        {
            try
            {
                if (Titulo != null && ValidarTexto(Titulo)
               && Usuario != null && ValidarTexto(Usuario)
               && Descripcion != null && ValidarTexto(Descripcion)
               && CobroHora != null && ValidarNumeros(CobroHora)
               && PersonaId != 0|| persona.PersonaId != 0)
                {

                    TuEmpleoEntities context = new TuEmpleoEntities();
                    perfil.PersonaId = persona.PersonaId;
                    perfil.Titulo = Titulo;
                    perfil.Usuario = Usuario;
                    perfil.Decripción = Descripcion;
                    perfil.CobroHora = Convert.ToDecimal(CobroHora);
                    context.Perfil.Add(perfil);
                    context.SaveChanges();
                    PerfilId = perfil.PerfilId;
                }
            }catch(Exception e)
            {
                return;
            }
        }
        public void EliminarPersona()
        {
            TuEmpleoEntities context = new TuEmpleoEntities();
            //Persona rm = context.Persona.Where(x => x.Nombre == Nombre&&
            //x.Apellido == Apellido&& x.Correo == Correo&& x.Contraseña == Contraseña&&
            // x.FechaNacimiento==Convert.ToDateTime(FechaNacimiento)&&x.Dni == Dni).FirstOrDefault();
            Persona p = context.Persona.Where(x => x.PersonaId == persona.PersonaId).First();

            context.Persona.Remove(p);
            context.SaveChanges();
        }
        public void ObtenerPerfil()
        {
            TuEmpleoEntities context = new TuEmpleoEntities();
            persona = context.Persona.Where(x => x.Correo.Equals(Correo) && x.Contraseña.Equals(Contraseña)).FirstOrDefault();
            perfil = context.Perfil.Where(x => x.PersonaId == persona.PersonaId).FirstOrDefault();
            return;
        }

        public bool ValidarTexto(String text)
        {
            if (Regex.IsMatch(text, @"^[a-zA-Z ]+$"))
            {
                return true;
            }
            return false;
        }

        public bool ValidarNumeros(String text)
        {
            if (Regex.IsMatch(text, @"^[0-9]+$"))
            {
                return true;
            }
            return false;
        }

        public bool ValidarFecha(String fecha)
        {
            DateTime datetime;
            if (!DateTime.TryParse(fecha, out datetime))
            {
                return false;
            }
            return true;
        }
        public bool ValidarCorreo(String correo)
        {
            try
            {
                MailAddress mail = new MailAddress(correo);
                using (TuEmpleoEntities context = new TuEmpleoEntities())
                {
                    if(context.Persona.Where(x=>x.Correo == correo).Count() != 0 )
                    {
                        return false;
                    }
                }
                    return true;
            }
            catch (FormatException)
            {
                return false;
            }

        }

    }
}