USE [TuEmpleo]
GO
/****** Object:  Table [dbo].[Perfil]    Script Date: 28/06/2017 03:34:11 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Perfil](
	[PerfilId] [int] IDENTITY(1,1) NOT NULL,
	[PersonaId] [int] NOT NULL,
	[Usuario] [varchar](15) NOT NULL,
	[Decripción] [varchar](200) NOT NULL,
	[Titulo] [varchar](50) NOT NULL,
	[CobroHora] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Perfil] PRIMARY KEY CLUSTERED 
(
	[PerfilId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Persona]    Script Date: 28/06/2017 03:34:11 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Persona](
	[PersonaId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Dni] [varchar](10) NOT NULL,
	[FechaNacimiento] [datetime] NOT NULL,
	[Correo] [varchar](50) NULL,
	[Contraseña] [varchar](25) NOT NULL,
 CONSTRAINT [PK_Persona] PRIMARY KEY CLUSTERED 
(
	[PersonaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Proyecto]    Script Date: 28/06/2017 03:34:11 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proyecto](
	[ProyectoId] [int] IDENTITY(1,1) NOT NULL,
	[Titulo] [varchar](50) NOT NULL,
	[Area] [varchar](20) NOT NULL,
	[Descripcion] [varchar](250) NOT NULL,
	[RangoPago] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Proyecto] PRIMARY KEY CLUSTERED 
(
	[ProyectoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProyectoPersona]    Script Date: 28/06/2017 03:34:11 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProyectoPersona](
	[ProyectoPersona] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoId] [int] NOT NULL,
	[PersonaId] [int] NOT NULL,
	[TipoPersonaId] [int] NOT NULL,
	[Mensaje] [varchar](200) NULL,
 CONSTRAINT [PK_ProyectoPersona] PRIMARY KEY CLUSTERED 
(
	[ProyectoPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoPersonaProyecto]    Script Date: 28/06/2017 03:34:11 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoPersonaProyecto](
	[TipoPersonaProyectoId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](15) NOT NULL,
 CONSTRAINT [PK_TipoPersonaProyecto_1] PRIMARY KEY CLUSTERED 
(
	[TipoPersonaProyectoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Perfil_Persona] FOREIGN KEY([PersonaId])
REFERENCES [dbo].[Persona] ([PersonaId])
GO
ALTER TABLE [dbo].[Perfil] CHECK CONSTRAINT [FK_Perfil_Persona]
GO
ALTER TABLE [dbo].[ProyectoPersona]  WITH CHECK ADD  CONSTRAINT [FK_ProyectoPersona_Persona] FOREIGN KEY([PersonaId])
REFERENCES [dbo].[Persona] ([PersonaId])
GO
ALTER TABLE [dbo].[ProyectoPersona] CHECK CONSTRAINT [FK_ProyectoPersona_Persona]
GO
ALTER TABLE [dbo].[ProyectoPersona]  WITH CHECK ADD  CONSTRAINT [FK_ProyectoPersona_Proyecto] FOREIGN KEY([ProyectoId])
REFERENCES [dbo].[Proyecto] ([ProyectoId])
GO
ALTER TABLE [dbo].[ProyectoPersona] CHECK CONSTRAINT [FK_ProyectoPersona_Proyecto]
GO
ALTER TABLE [dbo].[ProyectoPersona]  WITH CHECK ADD  CONSTRAINT [FK_ProyectoPersona_TipoPersonaProyecto] FOREIGN KEY([TipoPersonaId])
REFERENCES [dbo].[TipoPersonaProyecto] ([TipoPersonaProyectoId])
GO
ALTER TABLE [dbo].[ProyectoPersona] CHECK CONSTRAINT [FK_ProyectoPersona_TipoPersonaProyecto]
GO
