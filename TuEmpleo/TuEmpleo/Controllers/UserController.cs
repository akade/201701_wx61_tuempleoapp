﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TuEmpleo.Models;
using TuEmpleo.ViewModel;

namespace TuEmpleo.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            if (this.Session["Usuario"] == null) return RedirectToAction("Index","Home");
            //TempData["User"] = user;
            UsuarioViewModel user = (UsuarioViewModel)TempData["User"];
            TempData["User"] = user;
            ProyectoPuestoViewModel objViewModel = new ProyectoPuestoViewModel();
            return View(objViewModel);
        }
        public ActionResult VerProyecto(int id)
        {
            if (this.Session["Usuario"] == null) return RedirectToAction("Index", "Home");
            ProyectoDetalleViewModel objViewModel = new ProyectoDetalleViewModel();
            objViewModel.id = id;
            UsuarioViewModel user = (UsuarioViewModel)TempData["User"];
            TempData["User"] = user;

            objViewModel.Fill();
            objViewModel.UsuarioActual = user.perfil;
            TempData["DetalleView"] = objViewModel;
            return View(objViewModel);
        }

        public ActionResult AgregarPostulacion(ProyectoDetalleViewModel objViewModel)
        {
            if (this.Session["Usuario"] == null) return RedirectToAction("Index", "Home");
            UsuarioViewModel user = (UsuarioViewModel)TempData["User"];
            TempData["User"] = user;
            ProyectoDetalleViewModel temp = (ProyectoDetalleViewModel)TempData["DetalleView"];
            temp.MensajePostulacion = objViewModel.MensajePostulacion;
            temp.AgregarPostulacion();
            //objViewModel.AgregarPostulacion();
            return RedirectToAction("VerProyecto", new { id = objViewModel.Proyecto.ProyectoId });
        }

        public ActionResult AgregarProyecto(int id)
        {
            if (this.Session["Usuario"] == null) return RedirectToAction("Index", "Home");
            ProyectoUsuarioViewModel objViewModel = new ProyectoUsuarioViewModel();
            UsuarioViewModel user = (UsuarioViewModel)TempData["User"];
            TempData["User"] = user;
            objViewModel.PersonaID = user.persona.PersonaId;
            TempData["id"] = id;
            if (id > 0) objViewModel.ObtenerProyecto(id);
            if (objViewModel.ValidarCreador())
            {
                return View(objViewModel);
            }
            if(id == 0)
            {
                return View(objViewModel);
            }
            return RedirectToAction("VerProyectosCreados", "User");
        }

        public ActionResult GuardarProyecto(ProyectoUsuarioViewModel objViewModel)
        {
            try
            {
                if (this.Session["Usuario"] == null) return RedirectToAction("Index", "Home");
                UsuarioViewModel user = (UsuarioViewModel)TempData["User"];
                TempData["User"] = user;
                objViewModel.PersonaID = user.persona.PersonaId;
                objViewModel.GuardarProyecto();
                if(objViewModel.ProyectoID > 0)
                {
                    String MensajeRespuesta;
                    if ((int)TempData["id"] == 0)
                    {
                        MensajeRespuesta = "El proyecto se agregó correctamente";
                    }
                    else
                    {
                        MensajeRespuesta = "El proyecto se editó correctamente";
                    }                    
                    TempData["objMensaje"] = new KeyValuePair<String, String>("SUC", MensajeRespuesta);
                    ProyectoPuestoViewModel objViewModelBag = new ProyectoPuestoViewModel();
                    return View("Index", objViewModelBag);
                }
                else
                {
                    String MensajeRespuesta = "Ingrese los campos correctamente";
                    TempData["objMensaje"] = new KeyValuePair<String, String>("ERR", MensajeRespuesta);
                    int ide = (int)TempData["id"];
                    return RedirectToAction("AgregarProyecto",new { id= ide });
                }
               
            }
            catch(Exception ex)
            {
                String MensajeRespuesta = "Ocurrio un error al momento de grabar el proyecto";
                TempData["objMensaje"] = new KeyValuePair<String, String>("ERR", MensajeRespuesta);
                ProyectoPuestoViewModel objViewModelBag = new ProyectoPuestoViewModel();
                return View("Index", objViewModelBag);
            }

        }

        public ActionResult VerPostulaciones()
        {
            if (this.Session["Usuario"] == null) return RedirectToAction("Index", "Home");
            ProyectoPuestoViewModel objViewModel = new ProyectoPuestoViewModel();
            UsuarioViewModel user = (UsuarioViewModel)TempData["User"];
            TempData["User"] = user;
            objViewModel.persona = user.persona;
            objViewModel.ListaPorUsuario();
            return View(objViewModel);
        }

        public ActionResult VerProyectosCreados()
        {
            if (this.Session["Usuario"] == null) return RedirectToAction("Index", "Home");
            ProyectoPuestoViewModel objViewModel = new ProyectoPuestoViewModel();
            UsuarioViewModel user = (UsuarioViewModel)TempData["User"];
            TempData["User"] = user;
            objViewModel.persona = user.persona;
            objViewModel.ListaCreados();
            return View(objViewModel);
        }


    }
}