﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TuEmpleo.Models;
using TuEmpleo.ViewModel;

namespace TuEmpleo.Controllers
{
    public class HomeController : Controller
    {

        // GET: Home
        public ActionResult Index()
        {
            UsuarioViewModel objViewModel = new UsuarioViewModel();
            return View(objViewModel);
        }

        public ActionResult Home()
        {
            return View();
        }
        public ActionResult ValidarUsuario(UsuarioViewModel objViewModel)
        {
            TuEmpleoEntities context = new TuEmpleoEntities();
            var bd = context.Persona.Where(x => x.Correo == objViewModel.Correo && x.Contraseña == objViewModel.Contraseña).FirstOrDefault();
            if (bd != null)
            {
                objViewModel.persona = bd;
                objViewModel.ObtenerPerfil();
                if (objViewModel.perfil == null)
                {
                    TempData["Registro"] = objViewModel.persona;
                    String MensajeRespuesta = "Configure su perfil de Usuario";
                    TempData["objMensaje"] = new KeyValuePair<String, String>("SUC", MensajeRespuesta);
                    return View("CrearPerfil", objViewModel);
                }
                else
                {
                    String MensajeRespuesta = "Bienvenido";
                    TempData["objMensaje"] = new KeyValuePair<String, String>("SUC", MensajeRespuesta);
                    TempData["User"] = objViewModel;
                    this.Session["Usuario"] = bd;
                    return RedirectToAction("Index", "User");
                }

            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult RegistrarUsuario(UsuarioViewModel objViewModel)
        {
            //VALIDAR
            try
            {
                UsuarioViewModel temp = new UsuarioViewModel();
                temp = objViewModel;
                temp.GuardarPersona();
                if (temp.PersonaId > 0)
                {
                    TempData["Registro"] = temp.persona;
                    String MensajeRespuesta = "Configure su perfil de Usuario";
                    TempData["objMensaje"] = new KeyValuePair<String, String>("SUC", MensajeRespuesta);
                    return View("CrearPerfil", objViewModel);
                }
                else
                {
                    TempData["objMensaje"] = new KeyValuePair<String, String>("ERR",
                                       "Los campos ingresados no son validos.");
                    return RedirectToAction("Index", "Home");
                }
            }
            catch(Exception ex)
            {
                TempData["objMensaje"] = new KeyValuePair<String, String>("ERR",
                                       "Por favor intente más tarde.");
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult GuardarPerfil(UsuarioViewModel objViewModel)
        {
            try
            {
                
                objViewModel.persona = (Persona)TempData["Registro"];
                objViewModel.GuardarPerfil();
                
                if (objViewModel.PerfilId > 0)
                {
                    TempData["User"] = objViewModel;
                    this.Session["Usuario"] = objViewModel;
                    String MensajeRespuesta = "El usuario se registro correctamente.";
                    TempData["objMensaje"] = new KeyValuePair<String, String>("SUC", MensajeRespuesta);
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    TempData["objMensaje"] = new KeyValuePair<String, String>("ERR",
                                           "Los valores ingresados no son correctos");
                    TempData["Registro"] = objViewModel.persona;
                    return View("CrearPerfil",objViewModel);
                }
            }
            catch (Exception ex)
            {
                
                objViewModel.EliminarPersona();
                TempData["objMensaje"] = new KeyValuePair<String, String>("ERR",
                                       "Por favor intente más tarde.");
                return RedirectToAction("Index", "Home");
            }
        }

    }
}