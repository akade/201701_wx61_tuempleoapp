//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TuEmpleo.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Perfil
    {
        public int PerfilId { get; set; }
        public int PersonaId { get; set; }
        public string Usuario { get; set; }
        public string Decripción { get; set; }
        public string Titulo { get; set; }
        public decimal CobroHora { get; set; }
    
        public virtual Persona Persona { get; set; }
    }
}
