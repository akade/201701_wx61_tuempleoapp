﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TuEmpleo.Models;

namespace TuEmpleo.ViewModel
{
    public class ProyectoDetalleViewModel
    {
        public Proyecto Proyecto { get; set; }
        public List<ProyectoPersona> temp { get; set; }
        public List<Perfil> Postulantes { get; set; }
        public List<String> Mensajes { get; set; }
        public Perfil Creador { get; set; }

        public Perfil UsuarioActual { get; set; }
        public String MensajePostulacion { get; set; }

        public int id { get; set; }

        public ProyectoDetalleViewModel()
        {

            Postulantes = new List<Perfil>();
            Mensajes = new List<String>();
            Creador = new Perfil();
            UsuarioActual = new Perfil();

        }
        public void Fill()
        {

            TuEmpleoEntities context = new TuEmpleoEntities();
            Proyecto = context.Proyecto.Where(x => x.ProyectoId == id).First();
            temp = context.ProyectoPersona.Where(x => x.ProyectoId == id).ToList();
            foreach (var t in temp)
            {
                if (t.TipoPersonaProyecto.Nombre.Equals("Postulante"))
                {
                    Perfil perfil = new Perfil();
                    perfil = context.Perfil.Where(x => x.PersonaId == t.PersonaId).FirstOrDefault();
                    Postulantes.Add(perfil);
                    Mensajes.Add(t.Mensaje);
                }
                else
                {
                    Creador = context.Perfil.Where(x => x.PersonaId == t.PersonaId).FirstOrDefault();
                }
            }

        }
        public void AgregarPostulacion()
        {
            TuEmpleoEntities context = new TuEmpleoEntities();
            ProyectoPersona pp = new ProyectoPersona();
            if (MensajePostulacion != null)
            {
                pp.ProyectoId = Proyecto.ProyectoId;
            pp.PersonaId = UsuarioActual.PersonaId;
            pp.TipoPersonaId = 11;

            pp.Mensaje = MensajePostulacion;
            context.ProyectoPersona.Add(pp);
            context.SaveChanges();
            }
        }


    }
}